package com.dev.book.dto;

public enum Role {
    USER,
    ADMIN
}
